import React from 'react';
import ReactDOM from 'react-dom';
import Quotemachine from './Quotemachine.js';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Quotemachine />, div);
  ReactDOM.unmountComponentAtNode(div);
});
