import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Quotemachine.css';


function Quotemachine(props) {
    return (
      <div id="quote-box" className="Quotemachine">
        <h1 id="text">{ props.content }</h1>
        <h2 id="author">{ props.author }</h2>
        <div className="controls">
          <button id="new-quote" onClick={props.getQuote}>New Quote</button>
          <a id="tweet-quote" href={props.tweetUrl} target="_blank">
            <FontAwesomeIcon icon={['fab', 'twitter']} />
          </a>
        </div>
      </div>
    );
}

export default Quotemachine;
