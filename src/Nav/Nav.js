import React, { Component } from 'react';
import './Nav.css';

class Nav extends Component {
  render() {
    return (
      <header className="quote-header">
        <nav className="quote-nav">
          <h1>Random Quote Machine</h1>
        </nav>
      </header>
    );
  }
}

export default Nav;
