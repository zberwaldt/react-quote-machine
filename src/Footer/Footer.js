import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Footer.css';


function Footer() {
  return (
    <footer className="quote-footer">
      <p>Coded with <FontAwesomeIcon icon='heart' /> by <a rel="noopener noreferrer" href="https://github.com/zberwaldt" target="_blank" >Zach</a></p>
      <p>Check the <a rel="noopener noreferrer" target="_blank" href="https://github.com/zberwaldt/react-quote-machine">source</a> on github!</p>
    </footer>
  );
}

export default Footer;
