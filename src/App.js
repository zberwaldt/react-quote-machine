import React, { Component } from 'react';
import Nav from './Nav/Nav.js';
import Quotemachine from './Quotemachine/Quotemachine.js';
import Footer from './Footer/Footer.js';
import './App.css';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';

library.add(fab);
library.add(fas);


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      author: "Quote Author",
      content: "Quote Content"
    };

    this.quotes = [];
  }

  componentDidMount() {
   fetch('https://api.myjson.com/bins/ce9do')
      .then((resolve, reject) => {
        return resolve.json();
      })
      .then(data => {
        this.quotes = [...data.quotes];
      })
      .finally(() => {
        this.getRandomQuote();
      })
      .catch(err => console.log(err));
  }

  getRandomQuote() {
    let randInt = Math.floor(Math.random() * Math.floor(this.quotes.length));
    let quote = this.quotes[randInt];
    // check to see if the new quote is the same one, to prevent duplicates.
    if((quote.author === this.state.author) && (quote.content === this.state.content)) {
      randInt = Math.floor(Math.random() * Math.floor(this.quotes.length));
      quote = this.quotes[randInt];
    } else {
      this.setState({
        author: quote.author,
        content: quote.content
      });
    }
  }

  constructTwitterUrl() {
    return `https://twitter.com/intent/tweet?hashtags=random quote machine, quotes,freecodecamp,react&text=${encodeURIComponent(`"${this.state.content}"- ${this.state.author}`)}`;
  }

  render() {
    return (
      <div className="App">
        <Nav />
        <Quotemachine 
          author={this.state.author}
          content={this.state.content}
          tweetUrl={this.constructTwitterUrl()}
          getQuote={() => this.getRandomQuote()} />
        <Footer />
      </div>
    );
  }
}

export default App;
